/* ----------------------------- Activity2 - Session 23 ---------------------------------- */

/*
	1. Add the 6 users.
*/

db.users.insertMany([
	{
		"firstName": "Diane",
		"lastName": "Murphy",
		"email": "dmurphy@mail.com",
		"isAdmin": false,
		"isActive": true
	},
	{
		"firstName": "Mary",
		"lastName": "Patterson",
		"email": "mpatterson@mail.com",
		"isAdmin": false,
		"isActive": true
	},
	{
		"firstName": "Jeff",
		"lastName": "Firrelli",
		"email": "jfirrelli@mail.com",
		"isAdmin": false,
		"isActive": true
	},
	{
		"firstName": "Gerard",
		"lastName": "Bondur",
		"email": "gbondur@mail.com",
		"isAdmin": false,
		"isActive": true
	},
	{
		"firstName": "Pamela",
		"lastName": "Castillo",
		"email": "pcastillo@mail.com",
		"isAdmin": true,
		"isActive": false
	},
	{
		"firstName": "George",
		"lastName": "Vanauf",
		"email": "gvanauf@mail.com",
		"isAdmin": true,
		"isActive": true
	}
])


/*
	2. Add the 2 courses.
*/

db.courses.insertMany([
	{
		"name": "Professional Development",
		"price": 10000
	},
	{
		"name": "Business Processing",
		"price": 13000
	}
])


/*
	3. Get the users who are not administrators.
*/

db.users.find(
	{"isAdmin": false}
)


/*
	4. Get  users ID and add them as enrollees of the courses (update).
*/

db.courses.updateOne(
	{"price": 10000},
	{$set: {
			"enrollees": [
						ObjectId("620cc5fdf5ab9a6e7e4252f6"),
						ObjectId("620cc5fdf5ab9a6e7e4252f7"),
						ObjectId("620cc5fdf5ab9a6e7e4252f8")
					]
				}	
	}
)