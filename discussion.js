db.inventory.insertMany([
	{
		"name": "JS for beginners",
		"author": "James Doe",
		"price": 5000,
		"stocks": 50,
		"publisher": "JS Publishing House"
	},
	{
		"name": "HTML and CSS",
		"author": "John Thomas",
		"price": 2500,
		"stocks": 38,
		"publisher": "NY Publishers"
	},
	{
		"name": "Web Devt Fundamentals",
		"author": "Noah Jimenez",
		"price": 3000,
		"stocks": 10,
		"publisher": "Big Idea Publishing House"
	},
	{
		"name": "Java Programming",
		"author": "David Michael",
		"price": 10000,
		"stocks": 100,
		"publisher": "JS Publishing House"
	}
])

/*
	Comparison Query Operators

		$gt / $gte operator

		Syntax:
			db.collectionName.find({ field: {$gt: value }})
			db.collectionName.find({ field: {$gte: value }})

		$lt / $lte operator

		Syntax:
			db.collectionName.find({ field: {$lt: value }})
			db.collectionName.find({ field: {$lte: value }})

		$ne operator
			db.collectionName.find({ field: {$ne: value }})

		$eq operator
			db.collectionName.find({ field: {$eq: value }})

		$in operator
		     db.collectionName.find({ field: {$in: [value1, value2, ..] }})
*/

db.inventory.find({
	"stocks": {
		$gt: 50
	}
})

db.inventory.find({
	"stocks": {
		$gte: 50
	}
})

db.inventory.find({
	"stocks": {
		$lt: 50
	}
})

db.inventory.find({
	"stocks": {
		$lte: 50
	}
})

db.inventory.find({
	"stocks": {
		$ne: 50
	}
})

db.inventory.find({
	"price": {
		$in: [5000, 10000]
	}
})


/*
	Logical Query Operators

		$and operator

		Syntax:
			db.collectionName.find({
				$and: [ { "field": "value" }, { "field": "value" } ]
			})

		$or operator

		Syntax:
			db.collectionName.find({
				$or: [ { "field": "value" }, { "field": "value" } ]
			})
*/

db.inventory.find({
	$or: [
		{"name": "HTML and CSS"}, {"publisher": "JS Publishing House"}
	]
})

db.inventory.find({
	$and: [
		{"stocks": 100}, {"publisher": "JS Publishing House"}
	]
})


/*
	Field Projection

	Syntax:
		db.collectionName.find({ criteria } , {field: 1})
*/

db.inventory.find(
	{
		"publisher": "JS Publishing House"
	},
	{
		"_id": 0
		"name": 1,
		"author": 1
	}
)


/*
	Evaluation Query Operator

	$regex operator

	Syntax:
		db.collectionName.find({ field: {$regex: 'pattern', $options: 'optionsValue'}})
*/

// case sensitive
db.inventory.find({
	"author": {
		$regex: 'M'
	}
})

// case insensitive
db.inventory.find({
	"author": {
		$regex: 'M',
		$options: '$i'
	}
})